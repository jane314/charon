export type IIndexedObj = {
  [key: string]: {
    [col: string]: string;
  };
};

export function zip<T>(keys: string[], values: T[]): { [key: string]: T } {
  if (keys.length !== values.length) {
    console.error("error 6794505c: cannot zip arrays; lengths do not match");
    return {};
  }
  const obj: { [key: string]: T } = {};
  for (let i = 0; i < keys.length; i++) {
    obj[keys[i]] = values[i];
  }
  return obj;
}

export async function readCsvFileToGrid(filepath: string): Promise<string[][]> {
  try {
    const csv_text = await Deno.readTextFile(filepath);
    return csv_text.replaceAll("\r", "").split("\n").filter((str: string) =>
      !/^\s*$/.test(str)
    ).map(
      (row: string) => row.split(","),
    );
  } catch (e) {
    console.error(e.message);
    return [];
  }
}

export function gridToIndexedObj(
  grid: string[][],
  index_col: string,
): IIndexedObj {
  if (grid.length <= 1) {
    console.error("error 20e63402: empty grid data provided");
    return {};
  }
  const keys = grid[0];
  const index_index = keys.indexOf(index_col);
  if (index_index === -1) {
    console.error(`error 532f9788: invalid index_col ${index_col} provided`);
    return {};
  }
  const indexed_obj: IIndexedObj = {};
  for (let i = 1; i < grid.length; i++) {
    if (grid[i].length !== keys.length) {
      console.error(
        `error c246d953: skipping row ${i}; has different # of entries than grid row 1`,
      );
    } else {
      indexed_obj[grid[i][index_index]] = zip(keys, grid[i]);
    }
  }
  return indexed_obj;
}

export async function readCsvFileToIndexedObj(
  filepath: string,
  index_col: string,
): Promise<IIndexedObj> {
  const grid = await readCsvFileToGrid(filepath);
  return gridToIndexedObj(grid, index_col);
}
