/*
 * Types.
 */
export interface ITableEntry {
	[col: string]: number | string | Date | null;
};

export interface IAppConfig {
	columns: {
		[col: string]: {
			name: string;
			type: string;
			default: () => number | string | Date | null;
		}
	},
	views: {
		[view: string]: (data: ITableEntry[]) => ITableEntry[];
	}
}

/*
 * The main static data defining a Mydata instance.
 */
const app_static_data: IAppConfig = {
  columns: {
    description: {
      name: "Description",
      type: "string",
      default: () => "",
    },
    tags: {
      name: "Tags",
      type: "string",
      default: () => "",
    },
    "created_on": {
      name: "Created On",
      type: "Date",
      default: () => new Date(),
    },
    "last_edited": {
      name: "Last Edited",
      type: "Date",
      default: () => new Date(),
    },
    "popup_date": {
      name: "Popup Date",
      type: "Date",
      default: () => null,
    },
  },
  views: {
    Due: (data: ITableEntry[]): ITableEntry[] => {
      const today = new Date();
      return data.filter((entry: ITableEntry) => {
		  if(entry['popup_date']) {
			return (entry['popup_date'] <= today);
		  }
		  return false;
	  })
        .sort((a: ITableEntry, b: ITableEntry) => {
			if( !a['popup_date'] || ! b['popup_date'] ){
				return -1;
			} 
			return a['popup_date'] < b['popup_date'] ? 1 : -1
		});
    },
  },
};

export { app_static_data };
