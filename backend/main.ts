import { IAppConfig, app_static_data } from './app_setup.ts';

async function loadConfig(): Promise<IAppConfig | null> {
	const text = await Deno.readTextFile('./config.json');
	try {
		const obj: IAppConfig  = JSON.parse(text);
		return obj;
	} catch(e) {
		console.error(e.message);
		return null;
	}
}

const x = await loadConfig();
console.log(x);
